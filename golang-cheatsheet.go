// golang-cheatsheet.go

// Compilable Golang language syntax cheat-sheet

package main

import (
  "fmt"
)

//////////////////////////////////////////////////////////////////////////////

// Resources:

// https://golangr.com/

// https://golangdocs.com/maps-in-golang

// https://www.godesignpatterns.com/2014/04/new-vs-make.html

// https://golang.org/ref/spec#Making_slices_maps_and_channels

//////////////////////////////////////////////////////////////////////////////

// Variables:

func DoVariables() {

  a := 3
  b := "foo"

  var c string
  c = "foo2"

  fmt.Println( "a: ", a, ", b: ", b, ", c: ", c)

}

//////////////////////////////////////////////////////////////////////////////

// Loops:

func DoLoops() {

  for i := 0; 3 > i; i++ {
    fmt.Println( "i: ", i)
  }

  for ix, s := range []string{ "a", "b", "c"} {
    fmt.Println( "ix: ", ix, ", s: ", s)
  }

  i := 0
  for (3 > i) {
    fmt.Println( "i: ", i)
    i++
  }

}

//////////////////////////////////////////////////////////////////////////////

// Arrays:

func DoArrays() {

  var a = new([3]int)
  a[0] = 4
  a[1] = 5
  a[2] = 6
  b := [3] int { 7, 8, 9}

  fmt.Println( "a: ", a, ", b: ", b)

}

//////////////////////////////////////////////////////////////////////////////

// Slices:

func DoSlices() {

  preferred1 := []int{}

  preferred2 := make( [] int, 0)

  fmt.Println( "preferred1: ", preferred1, ", preferred2: ", preferred2)

  // From an existing array:
  a := [5] string { "a", "b", "c", "d", "e"}
  var s [] string
  s = a[0:2]
  var t [] string = a[3:5]

  // With data:
  u := [] int { 3, 4, 5, 6}

  // Specify the capacity and later load data:
  v := make( []int, 3, 20)
  v[0] = 10
  v[1] = 11
  v[2] = 12

  fmt.Println( "s: ", s, ", t: ", t, ", u: ", u, ", v: ", v)

}

//////////////////////////////////////////////////////////////////////////////

// Structs:

func DoStructs() {

  type aTime struct {
    h int
    m int
  }

  var start aTime
  start = aTime{ 9, 30}
  end  := aTime{ 10, 30}
  fmt.Println( "start: ", start, ", end: ", end)

  type interval struct {
    start aTime
    end   aTime
  }

  noonHour := interval{ aTime{ 12, 0}, aTime{ 13, 0}}
  fmt.Println( "noonHour: ", noonHour)

}

//////////////////////////////////////////////////////////////////////////////

// Maps:

func DoMaps() {

  caps := make(map[string]int)
  caps["a"] = 10
  caps["b"] = 11
  caps["c"] = 12

  fmt.Println( "caps: ", caps)

  vb, existsB := caps["b"]
  ve, existsE := caps["e"]

  fmt.Println( "b: ", vb, ", exists: ", existsB)
  fmt.Println( "e: ", ve, ", exists: ", existsE)

  delete( caps, "b")
  fmt.Println( "new caps: ", caps)

  for k, v := range caps {
    fmt.Println( "caps: k: ", k, ", v: ", v)
  }

}

//////////////////////////////////////////////////////////////////////////////

// Make vs. new:

func DoMakeVsNew() {

  type older struct {
    than int
    dirt int
  }

  // new(T) can create almost anything:
  p := new(older)
  fmt.Println( "p: ", p)
  p.than = 2
  p.dirt = 3

  fmt.Println( "p: ", p)

  // make(T) can only create slices, maps, and channels:
  aSlice   := make( []int, 3, 20)
  aMap     := make(map[string]int)
  aChannel := make( chan bool, 1000)

  fmt.Println( "aSlice: ", aSlice, ", aMap: ", aMap, ", aChannel: ", aChannel)

}

//////////////////////////////////////////////////////////////////////////////

func main() {
  DoVariables()
  DoLoops()
  DoStructs()
  DoArrays()
  DoSlices()
  DoMaps()
  DoMakeVsNew()
}

// The End.
